from flask import Flask,request,render_template,redirect,url_for,session,g
from elasticsearch import Elasticsearch, RequestsHttpConnection
import numpy as np
import os


app = Flask(__name__)
app.secret_key = os.urandom(24)


def get_tweets(es):

    res = es.search(index="labo", body={"size": 1, "query": {"match_all": {}}})

    #"match": {
    #    "polarity": "positive"
    #}


    #print("Got %d Hits" % res['hits']['total'])

    # Initialize the scroll
    doc = {
        'size': 20,
        'query': {
            'match_all': {}
        }
    }
    count = 1
    res = es.search(index='labo', doc_type='tweet', body=doc, scroll='4m')

    sid = res['_scroll_id']
    scroll_size = res['hits']['total']
    #print("Initialize the scroll size :", len(res['hits']['hits']))

    tweets = [hit["_source"]['text'] for hit in res['hits']['hits']]
    IDs = np.array([hit["_source"]['id'] for hit in res['hits']['hits']])

    # Start scrolling
    while (scroll_size < 50):
        print("Scrolling...")
        res = es.scroll(scroll_id=sid, scroll='4m')
        # Update the scroll ID
        sid = res['_scroll_id']
        # Get the number of results that we returned in the last scroll
        scroll_size = len(res['hits']['hits'])
        print("scroll size: " + str(scroll_size))
        # Do something with the obtained page
        tweets.append([hit["_source"]['text'] for hit in res['hits']['hits']])
        IDs.append([hit["_source"]['id'] for hit in res['hits']['hits']])
    return(IDs,tweets)

#@app.route("/")











def init_scroll(es):
    # Initialize the scroll
    doc = {
        'size': 5,
        'query': {
            'bool':{
            "must_not": {
                "exists": {
                    "field": "polarity"
                    }
                }
            }
            }
        }

    count = 1
    res = es.search(index='labo', doc_type='tweet', body=doc, scroll='4m')
    sid = res['_scroll_id']
    scroll_size = res['hits']['total']
    # print("Initialize the scroll size :", len(res['hits']['hits']))

    tweets = [hit["_source"]['text'] for hit in res['hits']['hits']]
    IDs = np.array([hit['_id'] for hit in res['hits']['hits']])
    return [sid,tweets,IDs]

def instantiate_elastic():
    try:
        es = Elasticsearch(
            ['137.74.41.51'],
            http_auth=('conseiletat', '5moreYears'),
            port=88,
            use_ssl=False,
            verify_certs=False,
            # ca_certs=certifi.where(),
            connection_class=RequestsHttpConnection,
        )
        return es
    except Exception as ex:
        print("Error:", ex)


es = instantiate_elastic()
globa_tweet = dict()
globa_sid = ""

def scroll(es,sid):
    print("Scrolling...")
    res = es.scroll(scroll_id=sid, scroll='99999999999m') # how long it should keep the “search context” alive  just needs to be long enough to process the previous batch of results.
    # Each scroll request (with the scroll parameter) sets a new expiry time.
    # Update the scroll ID

    new_sid = res['_scroll_id']
    # Get the number of results that we returned in the last scroll
    scroll_size = len(res['hits']['hits'])
    print("scroll size: " + str(scroll_size))
    # Do something with the obtained page
    tweets =[hit["_source"]['text'] for hit in res['hits']['hits']]
    IDs = [hit['_id'] for hit in res['hits']['hits']]
    dico = dict(zip(IDs, tweets))
    return [dico, new_sid]


@app.route("/index")
def index():

    sid, tweets, IDs = init_scroll(es) # init first scroll
    tweets_dico = dict(zip(IDs, tweets)) # first tweet dico

    set_global(sid,tweets_dico)

    return render_template("index.html", tweets=tweets_dico,sid=sid,user=session['user'])


@app.route('/',methods=['GET', 'POST'])
def login():
    if request.method =="POST":

        session.pop('user',None)

        if request.form['password'] == "password": # IF USER EXISTS
            session['user'] = request.form['username']

            sid, tweets, IDs = init_scroll(es)  # init first scroll
            tweets_dico = dict(zip(IDs, tweets))  # first tweet dico
            set_global(sid, tweets_dico)

            return render_template("index.html", tweets=tweets_dico, sid=sid,user=session['user'])

    return render_template('login.html')


@app.before_request
def before_request():
    g.user = None#almost like global variable
    if 'user' in session:
        g.user = session['user']

@app.route('/getsession')
def getsession():
    if 'user' in session:
        return session['user']

    return "Not logged in !"

@app.route('/dropsession')
def dropsession():
    session.pop('user',None)
    return render_template('login.html')

@app.route("/next_page" ,methods=['GET', 'POST'])
def next_page():

    sid = request.form['sid'] # get current sid
    print("******************** Next Page ******************")
    tup = scroll(es, sid)  #scroll_id may or may not be the same across subsequent scroll request. But the result set (hits) is diff in subsequent scroll calls
    set_global(tup[1], tup[0])
    return render_template('index.html',sid=tup[1],tweets=tup[0],user=session['user'])


def set_global(sid,tweets_dico):
    global globa_sid
    global globa_tweet
    globa_sid = sid
    globa_tweet = tweets_dico


@app.route("/polarity",methods=['POST'])
def polarity():

    
    tup = request.form['pola']
    pola = tup.split(",")[0]
    id = tup.split(",")[1]
    print("UPDATED ====> ID_ES ",id, "\t Polarity ",pola)

    #es.update(index='labo', doc_type='tweet', id=id,body={"doc": {"polarity": pola}})
    update_es(es,id,pola)
    return render_template('index.html', sid=globa_sid, tweets=globa_tweet,user=session['user'])


def update_es(es,id,pola): #met à jour aussi tous les tweets ressemblant 

    #Also make sure to enable scripting in elasticsearch.yml and restart ES:
    #script.inline: on  ,    script.indexed: on
    doc = {
       "_source":"text",
       "query": {
        "term" : {"_id":id} 
      }
    }
    res = es.search(index='labo', doc_type='tweet', body=doc)
    text = res['hits']['hits'][0]['_source']['text']
    
    if(pola=="objective"):
      body = {
       "script": {
         "inline": "ctx._source.polarity='objective'",
         "lang": "painless"
       },
       "min_score": 50,
       "query": {
         "bool": {
           "must": {
             "match" : {"text":text}
           }
         }
       }    
      }
      
    if(pola=="negative"):
      body = {
       "script": {
         "inline": "ctx._source.polarity='negative'",
         "lang": "painless"
       },
       "min_score": 90,
       "query": {
         "bool": {
           "must": {
             "match" : {"text":text}
           }
         }
       }
      }

    if(pola=="positive"):
      body = {
       "script": {
         "inline": "ctx._source.polarity='negative'",
         "lang": "painless"
       },
       "min_score": 90,
       "query": {
         "bool": {
           "must": {
             "match" : {"text":text}
           }
         }
       }
      }

   
    es.update_by_query(index="labo",doc_type="tweet",body=doc)


@app.route('/tuna')
def tuna():
    return '<h1> tuna is good </h1>'


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')









'''    <ul>
        {% for dict_item in tweets %}
            {% for key, value in dict_item.items() %}
                 <h1>Key: {{key}}</h1>
                 <h2>Value: {{value}}</h2>
            {% endfor %}
        {% endfor %}
    </ul>'''
